//import front.display.dao.DriverDAO;
//import front.display.dao.UserDAO;
//import front.display.dao.VehicleDAO;
//import front.display.model.Driver;
//import front.display.model.User;
//import front.display.model.Vehicle;
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
//
//import java.security.MessageDigest;
//import java.security.NoSuchAlgorithmException;
//import java.util.List;
//
//import static org.junit.Assert.assertEquals;
//import static org.junit.Assert.assertTrue;
//
///**
// * Created by Hans on 7.11.2014.
// */
//public class jUnitTest {
//
//    public static String md5(String text){
//        MessageDigest hash = null;
//        try {
//            hash = MessageDigest.getInstance("MD5");
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        }
//        hash.update(text.getBytes());
//        hash.update("9949".getBytes());
//
//        byte[] byteData = hash.digest();
//
//        StringBuffer sb = new StringBuffer();
//        for (int i = 0; i < byteData.length; i++) {
//            sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
//        }
//        return sb.toString();
//    }
//
//    ApplicationContext context;
//    UserDAO userDAO;
//    VehicleDAO vehicleDAO;
//    DriverDAO driverDAO;
//    User testUser;
//    Vehicle testVehicle;
//    Driver testDriver;
//
//    @Before
//    public void setUp(){
//        context = new ClassPathXmlApplicationContext("Beans.xml");
//        userDAO = (UserDAO) context.getBean("userDAO");
//        vehicleDAO = (VehicleDAO) context.getBean("vehicleDAO");
//        driverDAO = (DriverDAO) context.getBean("driverDAO");
//        testUser = new User(16, "test1", md5("test1"), "Test", "User", 20);
//        testVehicle = new Vehicle(2335, "246BDH", "246BDH", "2014-09-17 14:46:19.0", "811");
//        testDriver = new Driver(2, "Urmas Ott", "null", "null", 2391);
//    }
//    //Users
//    @Test
//    public void getUserByName(){
//        User user = userDAO.getUser("uname", "test1");
//        assertEquals(user, testUser);
//    }
//    @Test
//    public void getUserByID(){
//        User user = userDAO.getUser(16);
//        assertEquals(user, testUser);
//    }
//    @Test
//    public void getUserList(){
//        List<User> userList = userDAO.getUserList();
//        assertTrue(userList.contains(testUser));
//    }
//    //Vehicle
//    @Test
//    public void getVehicleByID(){
//        Vehicle vehicle = vehicleDAO.getVehicleByID(2335);
//        assertEquals(vehicle, testVehicle);
//    }
//    @Test
//    public void getVehicleByName(){
//        Vehicle vehicle = vehicleDAO.getVehicleByname("246BDH");
//        assertEquals(vehicle, testVehicle);
//    }
//    @Test
//    public void getVehicleByRegNo(){
//        Vehicle vehicle = vehicleDAO.getVehicleByRegNo("246BDH");
//        assertEquals(vehicle, testVehicle);
//    }
//    @Test
//    public void getVehicleList(){
//        List<Vehicle> vehicle = vehicleDAO.getVehicleList();
//        assertTrue(vehicle.contains(testVehicle));
//    }
//
//    //Driver
//    //Not comparing phone and email cause they are null in database
//    @Test
//    public void getDriverByName(){
//        List<Driver> driver = driverDAO.getDriverByName("Urmas Ott");
//        assertTrue(driver.contains(testDriver));
//    }
//    @Test
//     public void getDriverByVehicle(){
//        List<Driver> driver = driverDAO.getDriversByVehicle(2391);
//        assertTrue(driver.contains(testDriver));
//    }
//
//}
