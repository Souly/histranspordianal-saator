import com.thoughtworks.selenium.DefaultSelenium;
import com.thoughtworks.selenium.Selenium;
import junit.framework.TestCase;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class Adminpanel extends TestCase {
	private Selenium selenium;

	@Before
	public void setUp() throws Exception {
        selenium = new DefaultSelenium("localhost", 4444, "*firefox", "http://vrl.liblik.ee/");
        selenium.start();
	}

	@Test
	public void testAdminpanel() throws Exception {
		selenium.open("/signout");
		selenium.type("name=j_username", "test1");
		selenium.type("name=j_password", "test1");
		selenium.click("//button[@type='submit']");
		selenium.waitForPageToLoad("30000");
		selenium.click("link=Juhtpaneel");
		selenium.waitForPageToLoad("30000");
		selenium.click("name=edit");
		selenium.waitForPageToLoad("30000");
		selenium.type("name=j_email", "test1@gmail.com");
		selenium.click("name=save");
		selenium.waitForPageToLoad("30000");
		selenium.open("/signout");
	}

	@After
	public void tearDown() throws Exception {
		selenium.stop();
	}
}
