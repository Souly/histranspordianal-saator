import junit.framework.Test;
import junit.framework.TestSuite;

public class TestSuite1 {

	public static Test suite() {
		TestSuite suite = new TestSuite();
		suite.addTestSuite(Login.class);
		suite.addTestSuite(Adminpanel.class);
        suite.addTestSuite(RouteTextAndDownload.class);
		return suite;
	}

	public static void main(String[] args) {
		junit.textui.TestRunner.run(suite());
	}
}
