google.load("visualization", "1", {packages: ["corechart"]});
google.setOnLoadCallback(drawChart);
function drawChart() {
    var titles = (JSON.parse(localStorage["titles"]));
    var values = (JSON.parse(localStorage["values"]));
    console.log(values);
    var array = [];
    var t2 = [];
    t2.push(titles[0]);
    t2.push(titles[1]);
    array.push(t2);
    for (i = 0; i < values.length; i++) {
        var tmp = [];
        if (localStorage["valik"] == "tulp") {
            tmp.push(values[i][0]);
        } else if (localStorage["valik"] == "sektor") {
            tmp.push((values[i][0]));
        } else if (localStorage["valik"] == "punkt") {
            tmp.push(parseInt(values[i][0]));
        } else if (localStorage["valik"] == "joon") {
            tmp.push(parseInt(values[i][0]));
        }

        tmp.push(parseInt(values[i][1]));
        array.push(tmp);
    }
    console.log(array);
    var data = google.visualization.arrayToDataTable(array);

    if (localStorage["valik"] == "tulp") {
        var options = {
            title: 'TEST tulpdiagramm',
            hAxis: {title: titles[0], titleTextStyle: {color: 'black'}},
            vAxis: {title: titles[1], titleTextStyle: {color: 'black'}}
        };
    } else if (localStorage["valik"] == "sektor") {
        var options = {
            title: 'TEST sektordiagramm'
        };
    } else if (localStorage["valik"] == "punkt") {
        var options = {
            title: 'TEST punkt-diagramm',
            hAxis: {title: titles[0]},
            vAxis: {title: titles[1]},
            legend: 'none'
        };
    } else if (localStorage["valik"] == "joon") {
        var options = {
            title: 'TEST joon-diagramm',
            hAxis: {title: titles[0]},
            vAxis: {title: titles[1]}
        };
    }

    if (localStorage["valik"] == "punkt") {
        var chart = new google.visualization.ScatterChart(document.getElementById('chart_div'));
        console.log("punkt");
    } else if (localStorage["valik"] == "tulp") {
        var chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    } else if (localStorage["valik"] == "sektor") {
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
    } else if (localStorage["valik"] == "joon") {
        var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
    }


    chart.draw(data, options);
    localStorage.clear();

}
