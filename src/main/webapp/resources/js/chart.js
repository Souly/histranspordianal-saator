$(document).ready(function () {
    localStorage.clear();
    var tulp = document.getElementById("tulp");
    var sektor = document.getElementById("sektor");
    var punkt = document.getElementById("punkt");
    var joon = document.getElementById("joon");

    tulp.onclick = tulpFunk;
    sektor.onclick = sektorFunk;
    punkt.onclick = punktFunk;
    joon.onclick = joonFunk;

    function joonFunk() {
        localStorage.setItem("valik", "joon");
        funk();
    }

    function sektorFunk() {
        localStorage.setItem("valik", "sektor");
        funk();
    }

    function punktFunk() {
        localStorage.setItem("valik", "punkt");
        funk();
    }

    function tulpFunk() {
        localStorage.setItem("valik", "tulp");
        funk();
    }

    function funk() {

        console.log("ready");
        var titles = [];
        var asi = document.getElementById("table1").getElementsByTagName("th");
        for (i = 0; i < asi.length; i++) {
            console.log(asi.item(i).firstChild.nodeValue);
            titles.push(asi.item(i).firstChild.nodeValue);
        }
        console.log(titles);
        var values = [];
        var asi2 = document.getElementById("table1").getElementsByTagName("tbody").item(0);
        var asi3 = asi2.getElementsByTagName("tr");
        for (i = 0; i < asi3.length; i++) {
            var rida = asi3.item(i);
            var arr = [];
            for (j = 0; j < asi.length; j++) {
                var val = rida.getElementsByTagName("td").item(j).firstChild.nodeValue;
                arr.push(val);
            }
            values.push(arr);
        }
        console.log(values);
        localStorage.setItem("titles", JSON.stringify(titles));
        localStorage.setItem("values", JSON.stringify(values));

    }


});
