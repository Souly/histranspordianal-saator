<%@ page import="front.display.model.User" %>
<%--
  Created by IntelliJ IDEA.
  User: Hans
  Date: 11.10.2014
  Time: 15:33
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
    <title>Tartu &Uuml;histranspordianal&uuml;saator</title>
</head>
<body>
<div class="container">
    <% User user = (User) request.getAttribute("user"); %>
    <div class="row"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
        <form class="form-edit" role="form" method="post">
            <h2 class="form-edit-heading">Kasutaja ID ${user.getuId()} muutmine</h2>
            <input type="text" maxlength="20" class="form-control" name="j_username" placeholder="Kasutajatunnus" value="${user.getUname()}" required autofocus>
            <input type="text" maxlength="20" class="form-control" name="j_password" placeholder="Parool">
            <input type="text" maxlength="20" class="form-control" name="j_name" placeholder="Eesnimi" value="${user.getName()}" required>
            <input type="text" maxlength="20" class="form-control" name="j_surname" placeholder="Perenimi" value="${user.getSurname()}" required>
            <input type="text" maxlength="30"class="form-control" name="j_email" placeholder="E-mail">
            <input type="number" min="0" step="1" max="99999999" class="form-control" name="j_rights" placeholder="&Otilde;igused" value="${user.getRights()}" required>
            <input type=hidden name="id" value="${user.getuId()}">
            <button class="btn btn-lg btn-primary btn-block" name="save" type="submit">Muuda</button>
        </form>
        <form class="form-edit" role="form" method="post">
            <button class="btn btn-lg btn-primary btn-block" name="cancel" type="submit">Katkesta</button>
        </form>
    </div>
    <div class="col-md-4"></div>
</div> <!-- /container -->
</body>
</html>