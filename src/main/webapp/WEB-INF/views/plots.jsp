<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>

    <link href="<c:url value="/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">


    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/moment/min/moment-with-locales.min.js"></script>

    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/js/getCharts.js"></script>

    <title>Tartu &Uuml;histranspordianal&uuml;saator</title>


</head>
<body>
<div class="navbar-fixed top navbar-default navbar">

    <ul class="nav navbar-nav navbar-right">
        <li><a href="/main">Peaaken</a></li>
        <li class="active"><a href="/plots">Diagrammid</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-left">
        <li class="bg-info"><a href="/adminpanel">Juhtpaneel</a></li>
        <li  class="active"><a href="/signout">Logi v&auml;lja</a></li>
    </ul>
</div>
<div id="chart_div" style="width: 900px; height: 500px;"></div>

</body>

</html>