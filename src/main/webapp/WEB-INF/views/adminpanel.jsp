<%--
  Created by IntelliJ IDEA.
  User: Hans
  Date: 10.10.2014
  Time: 5:06
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>

    <link href="<c:url value="/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">


    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/moment/min/moment-with-locales.min.js"></script>

    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<%request.getContextPath();%>/resources/bootstrap/js/main.js"></script>

    <title>Tartu &Uuml;histranspordianal&uuml;saator</title>


</head>
<body>
    <div class="navbar-fixed top navbar-default navbar">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/main">MAIN</a></li>
            <li><a href="/plots">PLOTS</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-left">
            <li class="bg-info"><a href="/adminpanel">Juhtpaneel</a></li>
            <li  class="active"><a href="/signout">Logi v&auml;lja</a></li>
        </ul>
        <div class="form-group">
            <form method="post">
                <div class="container">
                    <p>
                    <div class="row">
                        <div class="col-md-3">
                            <form role="form" method="post"></form>
                                <div class="input-group">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">Username <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">Username</a></li>
                                            <li><a href="#">Firstname</a></li>
                                            <li><a href="#">ID</a></li>
                                        </ul>
                                    </div>
                                    <input type="hidden" class="getInfo" id="drop" name="drop" value="">
                                    <input type="text" class="form-control" placeholder="Value" name="value">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div>
        <c:if test="${not empty userList}">
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>Kasutaja ID</th>
                    <th>Kasutajanimi</th>
                    <th>Eesnimi</th>
                    <th>Perenimi</th>
                    <th>&Otilde;igused</th>
                </tr>
                </thead>
                <c:forEach var="o" items="${userList}">
                    <tr>
                        <td>${o.getuId()}</td>
                        <td>${o.getUname()}</td>
                        <td>${o.getName()}</td>
                        <td>${o.getSurname()}</td>
                        <td>${o.getRights()}</td>
                        <td>
                            <form role="form" method="POST">
                                <input type=hidden name="id" value="${o.getuId()}">
                                <button name="edit" type="submit">Muuda</button>
                            </form>
                        </td>
                        <td>
                            <form role="form" method="POST">
                                <input type=hidden name="id" value="${o.getuId()}">
                                <button name="delete" type="submit">Kustuta</button>
                            </form>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </div>
</body>

</html>

