<%--
  Created by IntelliJ IDEA.
  User: Silver
  Date: 4.10.2014
  Time: 12:08
  To change this template use File | Settings | File Templates.
--%>


<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
<head>

    <link href="<c:url value="/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />"
          rel="stylesheet">
    <link href="<c:url value="/resources/bower_components/bootstrap/dist/css/bootstrap.min.css" />" rel="stylesheet">


    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/bower_components/moment/min/moment-with-locales.min.js"></script>

    <script type="text/javascript"
            src="<%request.getContextPath();%>/resources/bower_components/jquery/dist/jquery.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/bower_components/js/main.js"></script>

    <!-- DataTables CSS -->
    <link href="<c:url value="/resources/datatable/css/jquery.dataTables.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/datatable/css/dataTables.tableTools.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/datatable/css/dataTables.colReorder.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/datatable/css/dataTables.colVis.css" />" rel="stylesheet">

    <!-- jQuery
    <script type="text/javascript" charset="utf8" src="<%request.getContextPath();%>/resources/datatable/js/jquery.js"></script>-->
    <!-- commented out, otherwise datetimepicker does not work, datatables work ATM-->

    <!-- DataTables -->
    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/datatable/js/jquery.dataTables.js"></script>

    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/datatable/js/main_datatable.js"></script>

    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/datatable/js/dataTables.tableTools.js"></script>

    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/datatable/js/dataTables.colReorder.js"></script>

    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/datatable/js/dataTables.colVis.js"></script>

    <script type="text/javascript" charset="utf8"
            src="<%request.getContextPath();%>/resources/js/chart.js"></script>

    <title>Tartu &Uuml;histranspordianal&uuml;saator</title>

</head>
<body>
<div class="navbar-fixed top navbar-default navbar">
    <div class="navbar-right">

        <ul class="nav navbar-nav">
            <li class="active"><a href="/main">Peaaken</a></li>
            <ul class="nav navbar-nav bg-info">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kuva kui <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a id="tulp" href="/plots">Tulpdiagramm</a></li>
                        <li><a id="sektor" href="/plots">Sektordiagramm</a></li>
                        <li><a id="punkt" href="/plots">Punkt-diagramm</a></li>
                        <li><a id="joon" href="/plots">Joon-diagramm</a></li>
                    </ul>
                </li>
            </ul>
        </ul>

    </div>
    <ul class="nav navbar-nav navbar-left">
        <li class="bg-info"><a href="/adminpanel">Juhtpaneel</a></li>
        <li class="active"><a href="/signout">Logi v&auml;lja</a></li>
    </ul>

    <div class="form-group">
        <form method="post">
            <div class="container"><p></div>
            <div class="container">
                <p/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Bussi nr</span>
                            <input type="number" class="form-control" placeholder="Bussi number" name="bus_number">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Liini nr</span>
                            <input type="text" class="form-control" placeholder="Liini number" name="line_number">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Juht</span>
                            <input type="text" class="form-control" placeholder="Bussijuhi nimi" name="bus_driver">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Peatus 1</span>
                            <input type="text" class="form-control" placeholder="Peatuse nimi" name="stop_name1">
                        </div>
                    </div>
                </div>
                <p/>

                <div class="row">
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Kiirus</span>
                            <input type="number" class="form-control" placeholder="Kiirus" name="speed">
                        </div>
                    </div>
                    <!--
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">lat</span>
                            <input type="text" class="form-control" placeholder="Laiuskraad (xx.xxxxxx)" name="lat">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">lon</span>
                            <input type="text" class="form-control" placeholder="Pikkuskraad (xx.xxxxxx)" name="lon">
                        </div>
                    </div>
                    -->
                    <div class="col-md-3">
                        <div class='input-group date' id='datetimepicker1'>
                            <span class="input-group-addon">Algus aeg</span>
                            <input type='datetime' class="form-control" name="startTime" readonly="readonly"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class='input-group date' id='datetimepicker2'>
                            <span class="input-group-addon">L&otilde;pp aeg</span>
                            <input type='text' class="form-control" name="endTime" readonly="readonly"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                    </span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="input-group">
                            <span class="input-group-addon">Peatus 2</span>
                            <input type="text" class="form-control" placeholder="Peatuse nimi" name="stop_name2">
                        </div>
                    </div>
                </div>
                <p/>


                <div class="row">
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary" name="palju">Otsi</button>
                    </div>
                </div>
            </div>

        </form>

    </div>

</div>

<div class="col-lg-10 col-lg-offset-1"><c:if test="${not empty search}">
    <table class="display" id="table1" cellspacing="0" width="100%">
        <thead>
        <tr>
            <th>Bussi number</th>
            <th>Liini number</th>
            <th>Bussijuht</th>
            <th>Peatus 1</th>
            <th>Peatus 2</th>
            <th>Kiirus</th>
            <th>Kuup&auml;ev</th>
            <th>Aeg</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="o" items="${search}">
            <tr>
                    <%--int bus_number, int line_number, String bus_driver, String initialStop,String finalStop, int speed, int reason, String date, int requestId, long timeTaken--%>
                <td>${o.bus_number}</td>
                <td>${o.line_number}</td>
                <td>${o.bus_driver}</td>
                <td>${o.initialStop}</td>
                <td>${o.finalStop}</td>
                <td>${o.speed}</td>
                <td>${o.date}</td>
                <td>${o.timeTaken}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</c:if></div>

</body>

</html>