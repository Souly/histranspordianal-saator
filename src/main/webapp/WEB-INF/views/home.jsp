<%@ taglib prefix="fn"
           uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="<c:url value="/resources/bootstrap/css/bootstrap.min.css" />" rel="stylesheet">
        <title>Tartu &Uuml;histranspordianal&uuml;saator</title>
    </head>
    <body>
        <div class="container">
            <div class="row"></div>
                <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <form class="form-signin" role="form" method="post">
                            <h2 class="form-signin-heading">Palun logi sisse</h2>
                            <input type="text" class="form-control" name="j_username" placeholder="Kasutajatunnus" required autofocus>
                            <input type="password" class="form-control" name="j_password" placeholder="Parool" required>
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Logi sisse</button>
                        </form>
                    </div>
                <div class="col-md-4"></div>

            </div>
        </div> <!-- /container -->
    </body>
</html>