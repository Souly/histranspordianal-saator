package server;

import front.display.dao.SearchDAOImpl;
import front.display.model.Response;
import front.display.model.Search;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Hans Peeter Tulmin on 5.12.2014.
 * <p/>
 * Class to parse queries.
 */
public class QueryParser {
	final static org.apache.log4j.Logger logger = Logger.getLogger(QueryParser.class);


	public static Map<Integer, ArrayList<Search>> mapResponses(ResultSet resultSet) throws SQLException {
		Map<Integer, ArrayList<Search>> resp = new HashMap<>();
		while (resultSet.next()) {

			Search search = new Search(
					//vehicle name
					resultSet.getInt("bus_nr"),
					resultSet.getString("driver_name"),
					resultSet.getString("stop"),
					resultSet.getInt("route"),
					resultSet.getInt("speed"),
					resultSet.getInt("reason"),
					resultSet.getString("request_time"),
					resultSet.getInt("request_id"));
			//Should take some other unique key
			int val = resultSet.getInt("trip_id");
			if (resp.get(val) == null) {
				ArrayList<Search> input = new ArrayList<>();
				input.add(search);
				resp.put(val, new ArrayList<>());
			}
			resp.get(val).add(search);
		}
		return resp;
	}

	//Probably need to introduce a filter to optimize
	public static ArrayList<Response> inferData(
			Map<Integer, ArrayList<Search>> map, String startStopConstraint, String endStopConstraint, Connection con) throws SQLException {
		ArrayList<Response> resp = new ArrayList<>();
		int count = 1;
		int speedSum = 0;
		int numberOfStopsFound = 1;
		long cumulativeSum = 0;
		ArrayList<Long> allElements = new ArrayList<>();
		long squareSum = 0;
		String startStop = null;
		boolean checkStart = true;
		Date startDate = new Date();
		for (Map.Entry<Integer, ArrayList<Search>> mapInput : map.entrySet()) {
			ArrayList<Search> input = mapInput.getValue();
			if (startStop == null)
				startStop = SearchDAOImpl.firstStop(input.get(0).getBus_number() + "", mapInput.getKey(), con);
			Collections.sort(input);
			long difference = 1;
			Date previousDate = null;
			for (Search search : input) {
				if (previousDate == null) {
					difference = 1;
				} else {
					difference = subtractDates(stringToDate(search.getDate()), previousDate);
				}
				previousDate = stringToDate(search.getDate());
				count += difference;
				speedSum += search.getSpeed() * difference;
				if (checkStart) {
					checkStart = false;
					startDate = stringToDate(search.getDate());

				}
				if (search.getStop_name() != null && !search.getStop_name().equals(startStop) && search.getSpeed() <6 && search.getReason() == 524288) {
					long timeTaken = subtractDates(stringToDate(search.getDate()), startDate);
					numberOfStopsFound++;
					//Just in case, Time is positive
					cumulativeSum += Math.abs(timeTaken);
					squareSum += Math.abs(timeTaken) * Math.abs(timeTaken);
					allElements.add(Math.abs((timeTaken)));
					Response
							tmp = new Response(search.getBus_number(), search.getLine_number(), search.getBus_driver(),
							startStop, search.getStop_name(), speedSum / count, search.getReason(),
							search.getDate(), search.getRequestId(), timeTaken);
					startStop = search.getStop_name();
					logger.debug(tmp.getInitialStop() + "=" + startStopConstraint + " and " + tmp.getFinalStop() + "=" + endStopConstraint + " also ");
					//We also discard the opening and closing of doors as a separate event.
/*					if ((tmp.getInitialStop() == null || tmp.getInitialStop().equals(startStopConstraint) || tmp.getInitialStop().equals("")) &&
							(tmp.getFinalStop() == null || tmp.getFinalStop().equals(endStopConstraint)
									|| tmp.getFinalStop().equals(""))) {*/
					if ((tmp.getInitialStop().equals(startStopConstraint) || startStopConstraint.equals("")) && (tmp.getFinalStop().equals(endStopConstraint)
							|| endStopConstraint.equals(""))) {
						resp.add(tmp);
					}
					speedSum = 1;
					count = 1;
					checkStart = true;

				}

			}

		}
		long mean = cumulativeSum/numberOfStopsFound;
		//long variance = ((squareSum - cumulativeSum * cumulativeSum) / numberOfStopsFound) / (numberOfStopsFound);
		//2 sigma threshold. We expect a gaussian distribution with some inherit bias we really don't care about, it is also distributed normally.
		//variance = squareSum/(numberOfStopsFound*numberOfStopsFound) - (cumulativeSum/numberOfStopsFound)* (cumulativeSum/numberOfStopsFound);
		long variance =0;
		for(long l : allElements) {
			variance+= (mean-l)*(mean-l);
		}
		variance = Math.round(Math.sqrt(Long.valueOf(variance).doubleValue()));
		variance = variance/numberOfStopsFound;
		long threshold = mean + 2 * variance;
		//Set the threshold for filtering purpose.
		SearchDAOImpl.setThreshold(threshold);
		return resp;
	}

	public static Date stringToDate(String date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date dt = null;
		try {
			dt = df.parse(date);
		} catch (ParseException e) {
			System.out.println("parse exception");
		}
		return dt;

	}

	private static long subtractDates(Date dt, Date dt2) {
		try {
			long dL1 = dt.getTime(); //time in millis
			long dL2 = dt2.getTime();
			//System.out.println(Math.abs((dL1 - dL2) / (1000))); //*60 = min, *60 = hours, *24 = days, *7 = days
			return Math.abs((dL1 - dL2) / (1000));
		} catch (NullPointerException e) {
			return 0;
		}
	}
}
