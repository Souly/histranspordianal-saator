package front.display.model;

/**
 * Created by Lauri on 7.10.2014.
 */
public class User extends Object {
	private int uId;
	private String uname;
	private String password;
	private String name;
	private String surname;
	private long rights;

	public User(int uId, String uname, String password, String name, String surname, long rights) {
		this.uId = uId;
		this.uname = uname;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.rights = rights;
	}

	public User(int uId, String uname, String password, String name, String surname) {
		this.uId = uId;
		this.uname = uname;
		this.password = password;
		this.name = name;
		this.surname = surname;
		this.rights = 1;
	}


	public User(int uId, String uname) {
		this.uId = uId;
		this.uname = uname;
	}

	public User(String uname, String password, String name, String surname) {
		this.uname = uname;
		this.password = password;
		this.name = name;
		this.surname = surname;
	}

	@Override
	public String toString() {
		return "User{" +
				"uId=" + uId +
				", uname='" + uname + '\'' +
				", name='" + name + '\'' +
				", password='" + password + '\'' +
				'}';
	}

    /*
    private int uId;
	private String uname;
    private String password;
    private String name;
    private String surname;
    private long rights;
     */

	public long getRights() {
		return rights;
	}

	public void setRights(long rights) {
		this.rights = rights;
	}

	public void setuId(int uId) {
		this.uId = uId;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getuId() {
		return uId;
	}

	public String getUname() {
		return uname;
	}

	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	@Override
	public boolean equals(Object other) {
		if (other == null) return false;
		if (other == this) return true;
		if (!(other instanceof User)) return false;
		User ot = (User) other;
		if (this.uId == ot.getuId() &&
				this.uname.equals(ot.getUname()) &&
				this.password.equals(ot.getPassword()) &&
				this.name.equals(ot.getName()) &&
				this.surname.equals(ot.getSurname()) &&
				this.rights == ot.getRights()) {

			return true;
		}
		return false;
	}

	@Override
	public int hashCode() {
		int result = uId;
		result = 31 * result + (uname != null ? uname.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (surname != null ? surname.hashCode() : 0);
		result = 31 * result + (int) (rights ^ (rights >>> 32));
		return result;
	}
}
