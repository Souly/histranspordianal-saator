package front.display.model;

/**
 * Created by Hans Peeter Tulmin on 11.12.2014.
 */
public class Response {

	private int bus_number; //812 for example
	private int line_number;       //12 for example
	private String bus_driver;  //Urmas Ott
	//	private String busType;     //OMNICITY  / BUS
	private String initialStop;
	private String finalStop;
//	private String stop_name;        //Stop name
	//	private int course;         //1-359 i guess
	private int speed;          //1-50gotagofast
	private int reason;
	private String date;
	private int requestId;
	private long timeTaken;

	public Response(int bus_number, int line_number, String bus_driver, String initialStop,String finalStop, int speed, int reason, String date, int requestId, long timeTaken) {
		this.bus_driver = bus_driver;
		this.line_number = line_number;
		this.bus_number = bus_number;
		this.initialStop = initialStop;
		this.finalStop = finalStop;
		this.speed = speed;
		this.reason = reason;
		this.date = date;
		this.requestId = requestId;
		this.timeTaken = timeTaken;
	}

	public int getBus_number() {
		return bus_number;
	}

	public void setBus_number(int bus_number) {
		this.bus_number = bus_number;
	}

	public int getLine_number() {
		return line_number;
	}

	public void setLine_number(int line_number) {
		this.line_number = line_number;
	}

	public String getBus_driver() {
		return bus_driver;
	}

	public void setBus_driver(String bus_driver) {
		this.bus_driver = bus_driver;
	}

	public String getInitialStop() {
		return initialStop;
	}

	public void setInitialStop(String initialStop) {
		this.initialStop = initialStop;
	}

	public String getFinalStop() {
		return finalStop;
	}

	public void setFinalStop(String finalStop) {
		this.finalStop = finalStop;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}
}
