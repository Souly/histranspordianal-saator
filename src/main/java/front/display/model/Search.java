package front.display.model;


public class Search implements Comparable<Search> {
	private int bus_number; //812 for example
	private int line_number;       //12 for example
	private String bus_driver;  //Urmas Ott
	//	private String busType;     //OMNICITY  / BUS
//	private int initialStop;
//	private int finalStop;
	private String stop_name;        //Stop name
	//	private int course;         //1-359 i guess
	private int speed;          //1-50gotagofast
	private int reason;
	private String date;
	private int requestId;


	public Search(int bus_number, String bus_driver, String stop_name, int line_number, int speed, int reason, String date, int requestId) {
		this.bus_number = bus_number;
		this.bus_driver = bus_driver;
		this.stop_name = stop_name;
		this.line_number = line_number;
		this.speed = speed;
		this.reason = reason;
		this.date = date;
		this.requestId = requestId;
	}

	public int getBus_number() {
		return bus_number;
	}

	public void setBus_number(int bus_number) {
		this.bus_number = bus_number;
	}

	public int getLine_number() {
		return line_number;
	}

	public void setLine_number(int line_number) {
		this.line_number = line_number;
	}

	public String getBus_driver() {
		return bus_driver;
	}

	public void setBus_driver(String bus_driver) {
		this.bus_driver = bus_driver;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}

	public int getSpeed() {
		return speed;
	}

	public void setSpeed(int speed) {
		this.speed = speed;
	}

	public int getReason() {
		return reason;
	}

	public void setReason(int reason) {
		this.reason = reason;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getRequestId() {
		return requestId;
	}

	public void setRequestId(int requestId) {
		this.requestId = requestId;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Search search = (Search) o;

		if (bus_number != search.bus_number) return false;
		if (line_number != search.line_number) return false;
		if (reason != search.reason) return false;
		if (requestId != search.requestId) return false;
		if (speed != search.speed) return false;
		if (bus_driver != null ? !bus_driver.equals(search.bus_driver) : search.bus_driver != null) return false;
		if (date != null ? !date.equals(search.date) : search.date != null) return false;
		if (stop_name != null ? !stop_name.equals(search.stop_name) : search.stop_name != null) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = bus_number;
		result = 31 * result + line_number;
		result = 31 * result + (bus_driver != null ? bus_driver.hashCode() : 0);
		result = 31 * result + (stop_name != null ? stop_name.hashCode() : 0);
		result = 31 * result + speed;
		result = 31 * result + reason;
		result = 31 * result + (date != null ? date.hashCode() : 0);
		result = 31 * result + requestId;
		return result;
	}

	//Compare by entryTime
	@Override
	public int compareTo(Search o) {
		if (this.requestId > o.requestId) return 1;
		else if (this.requestId < o.requestId) return -1;
		else return 0;
	}
}
