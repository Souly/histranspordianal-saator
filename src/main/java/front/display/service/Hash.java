package front.display.service;

import java.security.MessageDigest;

/**
 * Created by Hans Peeter Tulmin on 28.11.2014.
 */
public class Hash {

	public static String md5(String text) throws Exception {
		MessageDigest hash = MessageDigest.getInstance("MD5");
		hash.update(text.getBytes());
		hash.update("9949".getBytes());

		byte[] byteData = hash.digest();

		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < byteData.length; i++) {
			sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
		}

		return sb.toString();
	}
}
