package front.display.dao;


import front.display.model.Response;
import front.display.model.Search;
import server.QueryParser;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SearchDAOImpl implements SearchDAO {


	private DataSource dataSource;
	private static long threshold;

	public static void setThreshold(long threshold) {
		SearchDAOImpl.threshold = threshold;
	}

	public static String firstStop(String busNr, int tripId, Connection connection) {
		String resp = "";
		System.out.println(busNr + " and other is " + tripId);
		try {
			java.sql.PreparedStatement ps = connection.prepareStatement(
					"SELECT title \"title\" FROM trip_link_queue_out " +
							"JOIN trip_link_queue ON  trip_link_queue_out.trip_id = trip_link_queue.trip_id " +
							"JOIN Vehicles ON Vehicles.vid = trip_link_queue.vehicle_id " +
							"WHERE Vehicles.name = ? AND trip_link_queue_out.trip_id =? " +
							"GROUP BY trip_link_queue_out.title;");
			ps.setString(1, busNr);
			ps.setInt(2, tripId);
			ResultSet rs = ps.executeQuery();
			System.out.println("IS BEFORE FIRST "+rs.isBeforeFirst());
			if(rs.next()) resp = rs.getString("title");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return resp;
	}


	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}


	private String convertTime(String time) {
		if (time.length() == 15 || time.length() == 16) {
			String converted = time.substring(6, 10);
			converted += "-";
			converted += time.substring(3, 5);
			converted += "-";
			converted += time.substring(0, 2);
			converted += " ";
			if (time.length() == 15) {
				converted += "0";
				converted += time.substring(11, 12);
				converted += ":";
				converted += time.substring(13, 15);
			} else {
				converted += time.substring(11, 13);
				converted += ":";
				converted += time.substring(14, 16);
			}
			return converted;
		}
		return time;
	}

	@Override
	public List<Response> searchAll(String bus_number, String route, String bus_driver, String initialStop, String finalStop,
	                                String speed, String startTime, String endTime) {

		Map<Integer, ArrayList<Search>> resp;
		List<Response> response;
//        String AND = " AND ";
//        StringBuilder dbStatement = new StringBuilder();
		Connection conn = null;
		String command;
//	    = "SELECT * from factView WHERE bus_nr = ? AND driver_name = ? AND stop = ? AND speed = ? AND route = ? AND request_time = ?;";

		final String AND = " AND ";
		StringBuilder dbStatement = new StringBuilder();
		dbStatement.append("SELECT * FROM factView WHERE true");
		StringBuilder whereStatement = new StringBuilder();

		if (!bus_number.equals("")) {
			whereStatement.append(AND + "bus_nr = '" + bus_number + "'");
		}
		if (!bus_driver.equals("")) {
			whereStatement.append(AND + "driver_name = '" + bus_driver + "'");
		}
/*		if (!initialStop.equals("")) {
			whereStatement.append(AND + "stop = '" + initialStop + "'");
		}*/
		if (!route.equals("")) {
			whereStatement.append(AND + "route = '" + route + "'");
		}
		if (!startTime.equals("")) {
			whereStatement.append(AND + "request_time >= '" + convertTime(startTime) + "'");
		}
		if (!endTime.equals("")) {
			whereStatement.append(AND + "request_time <= '" + convertTime(endTime) + "'");
		}
		if (!speed.equals("")) {
			whereStatement.append(AND + "speed = '" + Integer.valueOf(speed) + "'");
		}

		command = dbStatement.toString() + whereStatement.toString();
		try {
			conn = dataSource.getConnection();
			java.sql.PreparedStatement ps = conn.prepareStatement(command);
//	        ps.setString(1,bus_number);
//	        ps.setString(2,bus_driver);
//	        ps.setString(3,initialStop);
//	        try{
//		        ps.setInt(4,Integer.parseInt((speed)));
//	        }catch (NumberFormatException e){
//		        ps.setInt(4,0);
//	        }
//	        ps.setString(5,route);
//	        long time = QueryParser.stringToDate(startTime).getTime();
//			ps.setTimestamp(6,new Timestamp(time));
//

//	        ("bus_nr" text, "driver_name" text, "stop" text, "speed" int, "reason" int, "route" text, "request_time" timestamp, "request_id" int, "trip_id" int)

			System.out.println(ps);
			ResultSet rs = ps.executeQuery();
			resp = QueryParser.mapResponses(rs);
			response = new ArrayList<Response>();
			for(Response r :QueryParser.inferData(resp, initialStop, finalStop, conn)) {
				if(5< r.getTimeTaken() && r.getTimeTaken()< threshold) {
					response.add(r);
				}
			}
			rs.close();
			return response;
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
				}
			}
		}
		return null;
	}

}
