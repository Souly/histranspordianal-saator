package front.display.dao;

import front.display.model.User;

import java.util.List;

/**
 * Created by Lauri on 7.10.2014.
 */
public interface UserDAO {

//	public void setDataSource(DataSource ds);
	public User getUser(int id);
    public User getUser(String column, String uname);
    public List<User> getUserList();
    public boolean putUser(User user);
    public boolean delUser(int uid);
    public boolean editUser(User user);


}
