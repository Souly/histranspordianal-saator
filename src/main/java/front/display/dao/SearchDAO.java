package front.display.dao;

import front.display.model.Response;

import java.util.List;

/**
 * Created by Stroheim on 6.11.2014.
 */
public interface SearchDAO {
//	private int bus_number; //812 for example
//	private String line_number;       //12 for example
//	private String bus_driver;  //Urmas Ott
//	//	private String busType;     //OMNICITY  / BUS
////	private int initialStop;
////	private int finalStop;
//	private String stop_name;        //Stop name
//	//	private int course;         //1-359 i guess
//	private int speed;          //1-50gotagofast
//	private Location location;
//	private String receive_time;   //
////	private String endTime;     //



	public List<Response> searchAll(String bus_number, String route,String bus_driver, String initialStop, String finalStop,
	                                String speed, String startTime, String endTime);


}
