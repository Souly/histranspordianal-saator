package front.display.controller;

import front.display.dao.UserDAO;
import front.display.model.User;
import front.display.service.Hash;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hans on 10.10.2014.
 */

@Controller
public class AdminPanelController {

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"})
	public ModelAndView listUsers(HttpServletRequest request) {
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}

		ApplicationContext context =
				new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");
		List<User> users = userDAO.getUserList();
		ModelAndView view = new ModelAndView("/adminpanel");
		view.addObject("userList", users);
		return view;
	}

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"}, method = RequestMethod.POST)
	public ModelAndView getUser(HttpServletRequest request) {

		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}
		String value = request.getParameter("value");
		String drop = request.getParameter("drop");
		ApplicationContext context =
				new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");
		List<User> users = new ArrayList<User>();
		if (drop.equals("Username") || drop.equals("")) {
			User dbUser = userDAO.getUser("uname", value);
			if (dbUser != null) {
				users.add(dbUser);
			}
		} else if (drop.equals("Firstname")) {
			List<User> dbUsers = userDAO.getUserList();
			for (User a : dbUsers) {
				if (a.getName().equalsIgnoreCase(value)) {
					users.add(a);
				}
			}
		} else if (drop.equals("ID")) {
			User dbUser = userDAO.getUser(Integer.parseInt(value));
			if (dbUser != null) {
				users.add(dbUser);
			}
		}
		ModelAndView view = new ModelAndView("/adminpanel");
		view.addObject("userList", users);
		return view;
	}

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"}, params = "edit", method = RequestMethod.POST)
	public ModelAndView editUser(HttpServletRequest request) {
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}
		ApplicationContext context =
				new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");
		int id = Integer.parseInt(request.getParameter("id"));
		User dbUser = userDAO.getUser(id);
		ModelAndView view = new ModelAndView("/edituser");
		view.addObject("user", dbUser);
		return view;
	}

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"}, params = "delete", method = RequestMethod.POST)
	public ModelAndView delUser(HttpServletRequest request) {
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}
		ApplicationContext context =
				new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");
		int id = Integer.parseInt(request.getParameter("id"));
		userDAO.delUser(id);
		return listUsers(request);
	}

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"}, params = "save", method = RequestMethod.POST)
	public ModelAndView saveUser(HttpServletRequest request) throws Exception {
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}
		ApplicationContext context =
				new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");
		int id = Integer.parseInt(request.getParameter("id"));
		long right = Long.parseLong(request.getParameter("j_rights"));
		User dbUser = userDAO.getUser(id);
		dbUser.setUname(request.getParameter("j_username"));
		dbUser.setName(request.getParameter("j_name"));
		dbUser.setSurname(request.getParameter("j_surname"));
		dbUser.setRights(right);
		if ((request.getParameter("j_password") != null) && (!request.getParameter("j_password").equals(""))) {
			dbUser.setPassword(Hash.md5(request.getParameter("j_password")));
		}
		userDAO.editUser(dbUser);
		return listUsers(request);
	}

	@RequestMapping(value = {"/adminpanel", "/adminpanel#"}, params = "cancel", method = RequestMethod.POST)
	public ModelAndView cancelUser(HttpServletRequest request) {
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return new ModelAndView("redirect:home");
		}
		if (sessUser.getRights() != 20) {
			return new ModelAndView("redirect:main");
		}
		return listUsers(request);
	}


}

