package front.display.controller;

import front.display.dao.UserDAO;
import front.display.model.User;
import front.display.service.Hash;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Hans on 9.10.2014.
 */

@Controller
public class RegisterController {
	@RequestMapping(value = "/register")
	public String home() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		User sessUser = (User) request.getSession().getAttribute("user");
		if (sessUser == null) {
			return "redirect:home";
		}
		if (sessUser.getRights() != 20) {
			return "redirect:main";
		}
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String register(HttpServletRequest request) throws Exception {
		String j_password = Hash.md5(String.valueOf(request.getParameter("j_password")));
		String j_username = String.valueOf(request.getParameter("j_username"));
		String j_name = String.valueOf(request.getParameter("j_name"));
		String j_surname = String.valueOf(request.getParameter("j_surname"));
		String j_email = String.valueOf(request.getParameter("j_email"));

		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		UserDAO userDAO = (UserDAO) context.getBean("userDAO");

		User user = new User(j_username, j_password, j_name, j_surname);
		User dbUser = userDAO.getUser("uname", j_username);
		System.out.println(j_password);
		//User dbUser2 = userDAO.getUser(j_email); //Test if user with same e-mail exists
		if (dbUser == null) {
			Boolean get = userDAO.putUser(user); //test if worked!
			return "redirect:main";
		} else {
			return "register";
		}
	}
}
