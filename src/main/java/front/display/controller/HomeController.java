package front.display.controller;

import front.display.dao.UserDAO;
import front.display.model.User;
import front.display.service.Hash;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by Hans Peeter Tulmin on 7.09.2014.
 */

@Controller
public class HomeController {

	@RequestMapping(value = {"/home", "", "/"})
	public String home() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			return "redirect:main";
		}
		return "home";
	}

	@RequestMapping(value = {"/home", "", "/"}, method = RequestMethod.POST)
	public String login(HttpServletRequest request) {
		try {
			String j_password = String.valueOf(request.getParameter("j_password"));
			String j_username = String.valueOf(request.getParameter("j_username"));
			String hashedPass = Hash.md5(j_password);

			ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
			UserDAO userDAO = (UserDAO) context.getBean("userDAO");

			User user = null;
			User dbUser = userDAO.getUser("uname", j_username);
			if (dbUser != null &&
					dbUser.getUname().equals(j_username) &&
					dbUser.getPassword().equals(hashedPass)) {
				user = dbUser;
			}
			if (user != null) {
				HttpSession session = request.getSession();
				session.setAttribute("user", user);
				session.setMaxInactiveInterval(43200); //12 hours 43200
				return "redirect:main";
			}
			return "home";
		}catch(Exception e) {
			//Fixing here
			return null;
		}

	}

	@RequestMapping(value = "/signout")
	public String signout() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			request.getSession().invalidate();
		}
		return "redirect:home";
	}


}


