package front.display.controller;

import front.display.model.Search;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Controller
public class XLSController {

	@RequestMapping(value = "/downloadXLS")
	public void downloadCSV(HttpServletResponse response, HttpSession session) throws IOException {
		try {
			List<Search> list = (ArrayList<Search>) session.getAttribute("list");
			String xlsFileName = session.getId();
			File file = new File(xlsFileName);
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Search");

			final Object[][] data = new Object[list.size() + 1][6];
			data[0] = new Object[]{"Bussi number", "Liini number", "Juhi nimi", "Peatuse nimi", "Kiirus", "Aeg"};

			for (int i = 0, k = 1; i < list.size(); i++, k++) {
				data[k] = new Object[]{list.get(i).getBus_number(), list.get(i).getLine_number(), list.get(i).getBus_driver(), list.get(i).getStop_name(), list.get(i).getSpeed(), ""};
			}

			for (int i = 0, k = 0; i < data.length; i++, k++) {
				Row row = sheet.createRow(k);
				Object[] objArr = data[i];
				int cellnum = 0;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					if (obj instanceof Date)
						cell.setCellValue((Date) obj);
					else if (obj instanceof Boolean)
						cell.setCellValue((Boolean) obj);
					else if (obj instanceof String)
						cell.setCellValue((String) obj);
					else if (obj instanceof Double)
						cell.setCellValue((Double) obj);
					else if (obj instanceof Integer)
						cell.setCellValue((Integer) obj);
				}

			}

			FileOutputStream out =
					new FileOutputStream(file);
			workbook.write(out);
			out.close();
			FileInputStream fis = new FileInputStream(file);
			response.setContentType("text/xls");
			response.setContentLength((int) file.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"search.xls\"",
					file.getName());
			response.setHeader(headerKey, headerValue);

			OutputStream outputStream = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			while ((bytesRead = fis.read(buffer)) != -1) {
				outputStream.write(buffer, 0, bytesRead);
			}

			fis.close();
			out.close();
			file.delete();

		} catch (NullPointerException e) {

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
