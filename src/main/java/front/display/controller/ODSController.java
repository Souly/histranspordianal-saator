package front.display.controller;

import front.display.model.Search;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Lauri on 4.11.2014.
 */

@Controller
public class ODSController {

	@RequestMapping(value = "/downloadODS", method = RequestMethod.GET)
	public void doDownload(HttpServletResponse response, HttpSession session) throws IOException {

		List<Search> list = (ArrayList<Search>) session.getAttribute("list");
		final Object[][] data = new Object[list.size()][4];
		for (int i = 0; i < list.size(); i++) {
			Search search = list.get(i);
			data[i] = new Object[]{list.get(i).getBus_number(), list.get(i).getLine_number(), list.get(i).getBus_driver(), list.get(i).getStop_name(), list.get(i).getSpeed(), ""};
		}
		String[] columns = new String[]{"Bussi number", "Liini number", "Juhi nimi", "Peatuse nimi", "Kiirus", "Aeg"};
		TableModel model = new DefaultTableModel(data, columns);
		String odsFileName = session.getId() + "data.ods";
		final File file = new File(odsFileName);
		SpreadSheet.createEmpty(model).saveAs(file);
		FileInputStream fis = new FileInputStream(file);

		response.setContentType("application/vnd.oasis.opendocument.spreadsheet");
		response.setContentLength((int) file.length());
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"routeData.ods\"",
				file.getName());
		response.setHeader(headerKey, headerValue);

		OutputStream out = response.getOutputStream();
		byte[] buffer = new byte[4096];
		int bytesRead = -1;

		while ((bytesRead = fis.read(buffer)) != -1) {
			out.write(buffer, 0, bytesRead);
		}

		fis.close();
		out.close();
		file.delete();
	}

}
