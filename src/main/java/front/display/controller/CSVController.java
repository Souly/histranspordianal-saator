package front.display.controller;

import front.display.model.Search;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.util.ArrayList;
import java.util.List;


@Controller
public class CSVController {

	@RequestMapping(value = "/downloadCSV")
	public void downloadCSV(HttpServletResponse response, HttpSession session) throws IOException {

		try {

			List<Search> list = (ArrayList<Search>) session.getAttribute("list");

			String CSV_SEPARATOR = ",";
			String csvFileName = session.getId();
			File file = new File(csvFileName);
			FileOutputStream fileOutputStream = new FileOutputStream(file);
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(fileOutputStream, "UTF-8");
			BufferedWriter bw = new BufferedWriter(outputStreamWriter);
			StringBuffer oneLine = new StringBuffer();
			oneLine.append("Bussi number");
			oneLine.append(CSV_SEPARATOR);
			oneLine.append("Liini number");
			oneLine.append(CSV_SEPARATOR);
			oneLine.append("Juhi nimi");
			oneLine.append(CSV_SEPARATOR);
			oneLine.append("Peatuse nimi");
			oneLine.append(CSV_SEPARATOR);
			oneLine.append("Kiirus");
			oneLine.append(CSV_SEPARATOR);
			oneLine.append("Aeg");
			bw.write(oneLine.toString());
			bw.newLine();
			for (Search search : list) {
				oneLine = new StringBuffer();
				oneLine.append(search.getBus_number());
				oneLine.append(CSV_SEPARATOR);
				oneLine.append(search.getLine_number());
				oneLine.append(CSV_SEPARATOR);
				oneLine.append(search.getBus_driver());
				oneLine.append(CSV_SEPARATOR);
				oneLine.append(search.getStop_name());
				oneLine.append(CSV_SEPARATOR);
				oneLine.append(search.getSpeed());
				oneLine.append(CSV_SEPARATOR);
				oneLine.append(" ");
				bw.write(oneLine.toString());
				bw.newLine();
			}
			bw.flush();
			bw.close();


			FileInputStream fis = new FileInputStream(file);
			response.setContentType("text/csv");
			response.setContentLength((int) file.length());

			String headerKey = "Content-Disposition";
			String headerValue = String.format("attachment; filename=\"search.csv\"",
					file.getName());
			response.setHeader(headerKey, headerValue);

			OutputStream out = response.getOutputStream();
			byte[] buffer = new byte[4096];
			int bytesRead = -1;

			while ((bytesRead = fis.read(buffer)) != -1) {
				out.write(buffer, 0, bytesRead);
			}

			fis.close();
			out.close();
			file.delete();
		} catch (NullPointerException e) {

		}

	}
}