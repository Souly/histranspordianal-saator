package front.display.controller;

import front.display.dao.SearchDAO;
import front.display.model.Response;
import front.display.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Scope;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by Stroheim on 6.10.2014.
 */

@Controller
@Scope("session")
public class MainController {

	@Autowired
	ApplicationContext context;

	@RequestMapping(value = "/main")
	public String home() {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			return "main";
		}
		return "redirect:/home";
	}


	@RequestMapping(value = "/main", method = RequestMethod.POST)
	public ModelAndView displayVehicle(HttpServletRequest request, HttpSession session) {


		String bus_number = request.getParameter("bus_number");
		String route = request.getParameter("line_number");
		String bus_driver = request.getParameter("bus_driver");
		String initialStop = request.getParameter("stop_name1");
		String finalStop = request.getParameter("stop_name2");
		String speed = request.getParameter("speed");
		String startTime = request.getParameter("startTime");
		String endTime = request.getParameter("endTime");
		context = new ClassPathXmlApplicationContext("Beans.xml");

		SearchDAO searchDAO = (SearchDAO) context.getBean("searchDAO");
		List<Response> list;
		list = searchDAO.searchAll(bus_number, route, bus_driver, initialStop, finalStop,
				speed, startTime, endTime);
		session.setAttribute("list", list);
		ModelAndView view = new ModelAndView("/main");
		view.addObject("search", list);
		return view;
	}

//	@RequestMapping(value = "/main", method = {RequestMethod.POST}, params = "palju")
//	public ModelAndView graphicMethod(){
//		return null;
//	}
//
//	@RequestMapping(value = "/main", method = {RequestMethod.POST}, params = "cleaner")
//	public ModelAndView cleanerMethod(){
//		return null;
//	}

}


