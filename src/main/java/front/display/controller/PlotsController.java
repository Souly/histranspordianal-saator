package front.display.controller;

import front.display.model.User;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.awt.image.RenderedImage;

/**
 * Created by Lauri on 3.11.2014.
 */

@Controller
public class PlotsController {

	@RequestMapping(value = "/plots")
	public ModelAndView goToHelloPage() {
		ModelAndView view = new ModelAndView("redirect:/home");
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		User user = (User) request.getSession().getAttribute("user");
		if (user != null) {
			view = new ModelAndView("/plots");
			return view;
		}
		DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		dataset.addValue(76, "Java", "Sandeep");
		dataset.addValue(30, "Java", "Sangeeta");
		dataset.addValue(50, "Java", "Surabhi");
		dataset.addValue(20, "Java", "Sumanta");
		dataset.addValue(10, "Oracle", "Sandeep");
		dataset.addValue(90, "Oracle", "Sangeeta");
		dataset.addValue(23, "Oracle", "Surabhi");
		dataset.addValue(87, "Oracle", "Sumanta");
		JFreeChart chart = ChartFactory.createAreaChart("Area chart", "", "Value", dataset, PlotOrientation.VERTICAL, true, true, false);
		RenderedImage chartImage = chart.createBufferedImage(300, 300);
		view.addObject(chartImage);
		return view;
	}
}
