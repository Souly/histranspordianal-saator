package schedulers;

/**
 * Created by Hans Peeter Tulmin on 5.11.2014.
 */

import database.ConnectionPooling;
import jobs.CopyJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

public class CronTrigger {
	public static void main(String[] args) throws Exception {
		//Quartz 1.6.3
		//JobDetail job = new JobDetail();
		//job.setName("dummyJobName");
		//job.setJobClass(HelloJob.class);
		ConnectionPooling connectionPooling = new ConnectionPooling();
		JobDetail job = JobBuilder.newJob(CopyJob.class)
				.withIdentity("dummyJobName", "group1").build();

		//Quartz 1.6.3
		//CronTrigger trigger = new CronTrigger();
		//trigger.setName("dummyTriggerName");
		//trigger.setCronExpression("0/5 * * * * ?");

		Trigger trigger = TriggerBuilder
				.newTrigger()
				.withIdentity("dummyTriggerName", "group1")
				.withSchedule(
						CronScheduleBuilder.cronSchedule("0/60 35 * * * ?"))
				.build();

		//schedule it
		Scheduler scheduler = new StdSchedulerFactory().getScheduler();
		scheduler.start();
		scheduler.scheduleJob(job, trigger);

	}
}