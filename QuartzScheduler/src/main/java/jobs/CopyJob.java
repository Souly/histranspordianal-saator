package jobs;

import database.CommandBuilder;
import database.ConnectionPooling;
import database.ReadProps;
import database.Table;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Hans Peeter Tulmin on 5.11.2014.
 */
public class CopyJob implements Job {

	@Override
	public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
		try (Connection src =
				     ConnectionPooling.getSourceConnection();
		     Connection sink = ConnectionPooling.getSinkConnection()) {
			ArrayList<Table> tables = ReadProps.createTables();
			System.out.println(tables.size());
			for (Table t : tables) {
				try {
					System.out.println(t.primaryKey + " " + t.keyValue);
					String command = CommandBuilder.buildGetCommand(t.sourceTable, t.sourceFields, t.primaryKey, t.keyValue,t.sourcePrimaryKeyType);
					//System.out.println("Command sent: " + command);
					PreparedStatement pst = src.prepareStatement(command);
					ResultSet rs = pst.executeQuery();
					//System.out.println("Command Executed. Startying copy");
					String insert = CommandBuilder.buildInsertHeader(t.sinkFields, t.sinkTable);
					boolean truthValue = rs.next();
					System.out.println(truthValue);
					String primaryKeyValue = t.keyValue;
					for (int i = 1; truthValue; i++) {
						System.out.println("I is : " + i);
						int resultIndex = 1;
						for (int j = 0; j < t.sourceTypes.length; j++) {
							System.out.println("j is: " + j + " and source len is: " + t.sourceFields.length);
							if (j == 0) {
								insert += "(";
							}
							//System.out.println("Trying to parse field");
							String input = rs.getString(resultIndex);
							System.out.println(input);
							System.out.println(t.sinkTypes[j]);
							if (t.sinkTypes[j].equals("point")) {
								resultIndex++;
								input += " " + rs.getString(resultIndex);
							}
							insert += CommandBuilder.parseField(input, t.sinkTypes[j]) + ", ";
							resultIndex++;
						}
						insert = insert.substring(0, insert.length() - 2) + ")";
						primaryKeyValue = rs.getString(t.primaryKey);
						//System.out.println("Primary key value: " + primaryKeyValue);
						truthValue = rs.next();
						if (i % 1 == 0 || !truthValue) {
							//System.out.println(insert);
							insert += ";";
							PreparedStatement add = sink.prepareStatement(insert);
							System.out.println(insert);
							add.execute();
							if (!truthValue) {
								break;
							}
							insert = CommandBuilder.buildInsertHeader(t.sinkFields, t.sinkTable);
						} else {
							insert += ",\n";
						}

					}
					ReadProps.setPropertyState(t.tableName, t.primaryKey, primaryKeyValue);
				}catch (SQLException e) {
					System.out.println("Malformed SQL, skipping table");
				}
			}
			System.out.println("Finished copy");
		} catch (SQLException e) {
			//e.printStackTrace();
			System.out.println("Malformed SQL");
		} catch (IOException e) {
			//e.printStackTrace();
		}
	}
}
