package database;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Hans Peeter Tulmin on 6.11.2014.
 */
public class ConnectionPooling {

	public static ComboPooledDataSource sourcePool = new ComboPooledDataSource();
	public static ComboPooledDataSource sinkPool = new ComboPooledDataSource();

	//True if we are working with the source, false if with the sink
	public ConnectionPooling() throws SQLException {
		ArrayList<String> props;
		try {
			System.out.println("Initializing connection pools");
			props = ReadProps.getDatabaseProps();
			sourcePool.setDriverClass(props.get(0));
			sourcePool.setJdbcUrl(props.get(1));
			sourcePool.setUser(props.get(2));
			sourcePool.setPassword(props.get(3));
			sinkPool.setDriverClass(props.get(4));
			System.out.println(props.get(4)+props.get(5)+props.get(6)+props.get(7));
			sinkPool.setJdbcUrl(props.get(5));
			sinkPool.setUser(props.get(6));
			sinkPool.setPassword(props.get(7));
			System.out.println("Connection pools initialized");
		} catch (IOException | PropertyVetoException  e) {
			System.out.println("Failed to create connection pools");
			DataSources.destroy(sourcePool);
			DataSources.destroy(sinkPool);
			e.printStackTrace();
		}
	}

	public static Connection getSourceConnection() throws SQLException {
		return sourcePool.getConnection();
	}

	public static Connection getSinkConnection() throws SQLException {
		return sinkPool.getConnection();
	}


}
