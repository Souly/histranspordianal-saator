package database;

/**
 * Created by Hans Peeter Tulmin on 6.11.2014.
 * Returns Strings given properties
 */
public class CommandBuilder {

	public static String buildGetCommand(String table, String[] fields, String key, String cur, String keyType) {
		System.out.println("Starting Command Building!");
		String command = "SELECT ";
		if (fields == null || fields.length == 0) {
			return "";
		}
		command += fields[0];
		for (int i = 1; i < fields.length; i++) {
			if (fields[i].split(" ").length > 1) {
				for (String s : fields[i].split(" ")) {
					command += "," + s;
				}
			} else {
				command += "," + fields[i];
			}
		}
		if (keyType.equals("date")) {
			command += " FROM " + table + " WHERE " + key + " > date('" + cur + "')";
		} else if (!keyType.equals("int")) {
			command += " FROM " + table + " WHERE " + key + " > '" + cur + "'";
		} else {
			command += " FROM " + table + " WHERE " + key + " > " + cur;
		}
		System.out.println("Command issued is: "+command);
		return command;
	}

	//Need to add point parsing as well. Add any data type here according to postgres query language.
	public static String parseField(String field, String type) {
		System.out.println("ParseField called with parameters field:" + field + " and type: " + type);
		if (field == null || field.equals("null")) {
			return "NULL";
		}
		if (type.equals("text")) {
			return "'" + field + "'";
		}
		try {
			if (field.split(" ").length == 2) {
				System.out.println("parsed point");
				//We have lat log in database but the POINT object is lon lat.
				return "ST_GeogFromText('POINT(" + Double.parseDouble(field.split(" ")[1]) + " " + Double.parseDouble(field.split(" ")[0]) + ")')";
			}
			return Integer.parseInt(field) + "";
		} catch (NumberFormatException e) {
			//Don't return anything if it is 0
			if (type.equals("int")) {
				return "NULL";
			}
			return "'" + field + "'";
		}
	}

	public static String buildInsertHeader(String[] strings, String table) {

		String insert = "SELECT merge_" + table.toLowerCase();//+" " + table + "(";
		/*for (int j = 0; j < strings.length; j++) {
			insert += strings[j];
			if (j != strings.length - 1) {
				insert += ", ";
			} else {
				insert += ") VALUES\n";
			}
		}*/
		return insert;
	}

	public static String buildCommand() {
		String resp = "";
		return resp;
	}
}
