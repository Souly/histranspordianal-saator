package database;

/**
 * Created by Hans Peeter Tulmin on 7.11.2014.
 */
public class Table {

	public String tableName;
	public String sourceTable;
	public String sinkTable;
	public String primaryKey;
	public String keyValue;
	public String[] sourceTypes;
	public String[] sinkTypes;
	public String[] sourceFields;
	public String[] sinkFields;
	public String sourcePrimaryKeyType;

	public Table(String tableName) {
		this.tableName = tableName;
	}

	public void evaluateKeyType() {
		for(int i=0;i<sourceFields.length;i++) {
			if(sourceFields[i].equals(primaryKey)) {
				this.sourcePrimaryKeyType = sourceTypes[i];
			}
		}
	}

	public void setStates(String states) {
		System.out.println(states);
		String[] stateArray = states.split(",");
		if(stateArray[0].equals("NAN")) {
			this.primaryKey="NAN";
			this.keyValue="NAN";
		} else {
			this.primaryKey=stateArray[0];
			this.keyValue = stateArray[1];
		}
		System.out.println("Added states");

	}
	public void setMappings(String mappings) {
		String[] mappingArray = mappings.split(",");
		this.sourceTable=mappingArray[0];
		this.sinkTable=mappingArray[1];
		System.out.println("Added mappings");
	}

	public void setMapType(String mapType) {
		String[] mapTypeArray = mapType.split(";");
		this.sourceTypes=mapTypeArray[0].split(",");
		this.sinkTypes=mapTypeArray[1].split(",");
		System.out.println("Added mapping types");
	}

	public void setMapConf(String mapConf) {
		String[] mapConfArray = mapConf.split(";");
		this.sourceFields=mapConfArray[0].split(",");
		this.sinkFields=mapConfArray[1].split(",");
		System.out.println("Added mapping configurations");
	}


	public Table(String tableName,String states, String mapping,String mapType, String mapConf) {
		System.out.println(mapping);
		System.out.println(mapType);
		System.out.println(mapConf);
		String[] stateArray = states.split(",");
		String[] mappingArray = mapping.split(",");
		String[] mapTypeArray = mapType.split(";");
		String[] mapConfArray = mapConf.split(";");
		System.out.println(stateArray.length+" "+mappingArray.length+ " "+mapTypeArray.length+ " "+mapConfArray.length);
		//We overwrite the whole table in this case
		this.tableName = tableName;
		if(stateArray[0].equals("NAN")) {
			this.primaryKey="NAN";
			this.keyValue="NAN";
		} else {
			this.primaryKey=stateArray[0];
			this.keyValue = stateArray[1];
		}
		this.sourceTable=mappingArray[0];
		this.sinkTable=mappingArray[1];
		this.sourceTypes=mapTypeArray[0].split(",");
		this.sourceFields=mapConfArray[0].split(",");
		this.sinkTypes=mapTypeArray[1].split(",");
		this.sinkFields=mapConfArray[1].split(",");
		System.out.println("Table Created");
	}
}
