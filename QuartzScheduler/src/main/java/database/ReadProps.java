package database;

import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by Hans Peeter Tulmin on 6.11.2014.
 */
public class ReadProps {

	public static ArrayList<String> getDatabaseProps() throws IOException {
		ArrayList<String> resp = new ArrayList<>();
		String fileName = "connections.properties";
		InputStream is = new FileInputStream(fileName);
		Properties prop = new Properties();
		if (is == null) {
			throw new IOException("Property file " + fileName + " not found in source. Please add such a file");
		}
		prop.load(is);
		//Load source data if true, else load sink data
		resp.add(prop.getProperty("sourceDriverClassName"));
		resp.add(prop.getProperty("sourceUrl"));
		resp.add(prop.getProperty("sourceUsername"));
		resp.add(prop.getProperty("sourcePassword"));
		resp.add(prop.getProperty("sinkDriverClassName"));
		resp.add(prop.getProperty("sinkUrl"));
		resp.add(prop.getProperty("sinkUsername"));
		resp.add(prop.getProperty("sinkPassword"));
		return resp;
	}

	public static ArrayList<Table> createTables() throws IOException {
		System.out.println("Reading properties to create Tables");
		ArrayList<Table> resp = new ArrayList<>();
		String stateProps = "states.properties";
		String mapProps = "mappings.properties";
		InputStream stateIs = new FileInputStream(stateProps);
		InputStream mapIs = new FileInputStream(mapProps);
		if (stateIs == null || mapIs == null) {
			throw new IOException("Failed reading either mappings.properties or states.properties");
		}
		BufferedReader br = new BufferedReader(new InputStreamReader(stateIs));
		BufferedReader mapReader = new BufferedReader(new InputStreamReader(mapIs));
		String line;
		while ((line = br.readLine()) != null) {
			if(line.startsWith("#")) {
				continue;
			}
			Table t = new Table(line.split("=")[0]);
			t.setStates(line.split("=")[1]);
			resp.add(t);

		}
		while((line = mapReader.readLine())!= null) {
			if(line.startsWith("#")) {
				continue;
			}
			String head = line.split("=")[0];
			if(head.substring(head.length()-4,head.length()).equals("conf")) {
				for(Table t : resp) {
					if(t.tableName.equals(head.substring(0,head.length()-4))) {
						t.setMapConf(line.split("=")[1]);
						break;
					}
				}
			}else if(head.substring(head.length()-3,head.length()).equals("key")) {
				for(Table t : resp) {
					if(t.tableName.equals(head.substring(0,head.length()-3))) {
						t.setMapType(line.split("=")[1]);
						break;
					}
				}
			}else {
				for(Table t : resp) {
					System.out.println(head);
					if(t.tableName.equals(head)) {
						t.setMappings(line.split("=")[1]);
						break;
					}
				}
			}


		}
/*			String s = line.split("=")[1];
			String tableName = line.split("=")[0];
			if(line.split("=")[0].equals(tableName)) {

			String s1 = mapReader.readLine().split("=")[1];
			}
			String s2 = mapReader.readLine().split("=")[1];
			String s3 = mapReader.readLine().split("=")[1];
			System.out.println(s);
			System.out.println(s1);
			System.out.println(s2);
			System.out.println(s3);
			Table t = new Table(tableName,s,
					s1,
					s2, s3);
			resp.add(t);

		System.out.println("Tables created");*/
		//Have to do in the end, else we might have issues with not evaluated fields.
		for(Table t : resp) {
			t.evaluateKeyType();
		}
		return resp;
	}

	public static void setPropertyState(String tableName,String key, String lastElement) {
		System.out.println("Setting property states");
//		String stateProps = "states.properties";
		try {
/*
			FileInputStream stateIs = new FileInputStream(stateProps);
			Properties properties = new Properties();
			properties.load(stateIs);
			stateIs.close();
			String tmp = properties.getProperty(key);
			String[] tmpArray = tmp.split(",");
			String newValue = "";
			for(int i = 0;i<tmpArray.length-1;i++) {
				newValue+=tmpArray[i]+",";
			}
			newValue+=lastElement;
			PropertiesConfiguration config = new PropertiesConfiguration(stateProps);
			properties.setProperty(key,newValue);
			System.out.println("newval:" + newValue);
			System.out.println("key:"+key);
			config.save();
*/
			System.out.println("Updating property state, tablename is: "+tableName+" key is: "+key+" and lastelement: "+lastElement);
			FileInputStream fis = new FileInputStream("states.properties");
			Properties props = new Properties();
			props.load(fis);
			fis.close();
			FileOutputStream fos = new FileOutputStream("states.properties");
			props.setProperty(tableName,key+","+lastElement);
			props.store(fos, null);
			fos.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
